var gulp = require('gulp');
var sass = require('gulp-sass');
var imagemin = require('gulp-imagemin');
var csso = require('gulp-csso');
var browserSync = require('browser-sync');
var concat = require('gulp-concat');
var jsMinify = require('gulp-minify');
var callback = require('gulp-callback');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var envify = require('loose-envify/custom');
var buffer = require('vinyl-buffer');

var config = require('./config');

var task = {
	compileJs: function(){

		if(!config.jsIncludesMore.length) return;

		function compile(obj) {
			var src = obj.src;
			var name = obj.name + config.compile + '.js';
			return browserify({
                extensions: ['.jsx', '.js'],
                entries: src

            })
			.transform('babelify', {presets: ['es2015', 'react']})
			.transform(
				envify({
					_: 'purge', NODE_ENV: 'production'
				}), { global: true })
			.bundle()
			.on("error", function (err) { console.log("Error : " + err.message); })
			.pipe(source(name))
			.pipe(buffer())
			.pipe(gulp.dest(config.src + config.jsDir));
        }

        for(var i=0; i<config.jsIncludesMore.length-1; i++){
            compile(config.jsIncludesMore[i]);
        }

		return compile(config.jsIncludesMore[i]);
	},
	compileScss: function(){
		return gulp.src(config.cssIncludes)
		.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
		.pipe(gulp.dest(config.src + config.cssDir));

		function compile(obj){
			return gulp.src(obj.src)
			.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
			.pipe(gulp.dest(config.src + config.cssDir));
		}

		for(var i=0; i<config.cssIncludesMore.length-1; i++){
            compile(config.cssIncludesMore[i]);
        }

		return compile(config.cssIncludesMore[i]);
	},
	compressImg: function(){
		var srcImport = config.src + config.imgDir + '/**/*';
		var srcExport = config.export + config.imgDir
		return gulp.src([srcImport + '.jpg', srcImport + '.png', srcImport + '.gif'])
	  	.pipe(imagemin())
	 	.pipe(gulp.dest(srcExport));
	},
	destSvg: function(){
		var srcImport = [config.src + config.imgDir + '/**/*.svg', config.src + config.imgDir + '/*.svg'];
		var srcExport = config.export + config.imgDir
		return gulp.src(srcImport)
  		.pipe(gulp.dest(srcExport))
	},
	compressStyles: function(){
		var src = config.src + config.cssDir + '/*.css';
        var srcExport = config.export + config.cssDir;
		return gulp.src(src)
			.pipe(csso())
			.pipe(gulp.dest(srcExport));

	},
	destHtml: function(){
		return gulp.src(config.src + '/**/*.html')
  		.pipe(gulp.dest(config.export));
	},
	compressJs: function(){
		var srcImport = config.src + config.jsDir + '/*.js';
        var srcExport = config.export + config.jsDir;

		return gulp.src(srcImport)
			.pipe(jsMinify({
				ext:{
					min: config.min + '.js'
				}
			}))
			.pipe(gulp.dest(srcExport));
	},
	start: function(){
		task.compileScss();
  		task.compileJs();
	}
}

gulp.task('start', task.start);

gulp.task('compile', task.start);

gulp.task('compileJs', task.compileJs);

gulp.task('compileScss',task.compileScss);

gulp.task('compress', function() {

  task.compressImg();

  task.destSvg();

  task.compileScss().pipe(callback(function() {
      task.compressStyles();
  }));

  task.compileJs().pipe(callback(function() {console.log()
     task.compressJs()
  }));

  task.destHtml();

});

gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browserSync
        server: { // Определяем параметры сервера
            baseDir: 'app' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});

gulp.task('runDist', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browserSync
        server: { // Определяем параметры сервера
            baseDir: 'dist' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});

gulp.task('watch', ['start','browser-sync'], function () {

	function editPaths(arr){
		for(var i=0; i<arr.length; i++){
			var elArr = arr[i].split('/');
			elArr[elArr.length-1] = '**/*';
			arr[i] = elArr.join('/');
		}
		return arr;
	}

	var cssDirs = editPaths(config.cssIncludes.slice());
	var jsDirs = editPaths(config.jsIncludes.slice());

	gulp.watch(cssDirs).on('change', function(){
        task.compileScss().pipe(callback(browserSync.reload));
    });
	gulp.watch(jsDirs).on('change', function(){
		task.compileJs().pipe(callback(browserSync.reload));
	});
	gulp.watch(config.src + '/*').on('change', browserSync.reload);
});
