/**
 * Routes app
 */

import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import App from './components/App';
import ALL_REDUCERS from './reducers';

const ROOT = document.getElementById('appRoutes');
const STORE = createStore(ALL_REDUCERS);

ReactDOM.render(
	<Provider store={STORE}>
		<App />
	</Provider>,
    ROOT
);
