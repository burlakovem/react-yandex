/**
 * App body
 */

import React from 'react';
import SearchField from './SearchField';
import Ymap from '../containers/Ymap';
import RoutesContent from '../containers/RoutesContent';


class App extends React.Component{

	constructor(props){
		super();
	}

	render(){
		return (
			<div className="appBody">
				<div className="appBody__left routesContent">
					<SearchField />
					<RoutesContent />
				</div>
				<div className="appBody__right yandexMap">
					<Ymap />
				</div>
			</div>

		)
	}

}

export default App;