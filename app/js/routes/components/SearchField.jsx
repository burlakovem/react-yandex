/**
 * Search field from created new route
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { YMaps } from 'react-yandex-maps';
import { addRoute } from '../actions';
import LastQueries from '../containers/LastQueries';


class SearchField extends React.Component{

    constructor(props){
        super();
        this.searchRef = React.createRef();
        this.submit = this.submit.bind(this);
        this.toggleViewLastQueries = this.toggleViewLastQueries.bind(this);
        this.state = {
            openLastQueries: false
        }
    }

    /**
     * Setting src to ymaps object
     * @param ymaps
     */
    setYmapsThis(ymaps){
        this.ymaps = ymaps;
        var suggestView = new ymaps.SuggestView(this.searchRef.current);
    }

    /**
     * Created new route
     * @param e
     */
    submit(e){
        e.preventDefault();
        var search = $(this.searchRef.current);
        var val = search.val();
        this.props.addRoute(val);
        search.val('');
    }

    /**
     * Render last queries
     * @return {XML}
     */
    renderLastQueries(){
        if(this.state.openLastQueries){
            return <LastQueries toggleViewLastQueries={this.toggleViewLastQueries} />
        }
    }

    /**
     * Toggle (show/hide) last queries
     * @param e
     */
    toggleViewLastQueries(e){
        if(e){
            e.preventDefault();
        }
        this.setState((prevState, props) => ({
            openLastQueries: !this.state.openLastQueries
        }))
    }

    render(){
        return (
            <form onSubmit={this.submit} className="search routesContent__search">
                <YMaps onApiAvaliable={ymaps => this.setYmapsThis(ymaps)}></YMaps>
                <input ref={this.searchRef} type="text" className="search__input" placeholder="Новая точка маршрута" />
                <span onClick={this.toggleViewLastQueries} className="pointer search__pointer"></span>
                <button className="lupa search__lupa" ></button>
                {this.renderLastQueries()}
            </form>
        )
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        addRoute: addRoute
    },dispatch)
}

export default connect(null, matchDispatchToProps)(SearchField);