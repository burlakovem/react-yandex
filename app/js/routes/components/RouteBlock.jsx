/**
 * Route block for list
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { removeRoute, updateRoute } from '../actions';


class RouteBlock extends React.Component{

	constructor(props){
		super();
        this.refAddressField = React.createRef();
		this.startAddressEditing = this.startAddressEditing.bind(this);
		this.endAddressEditing = this.endAddressEditing.bind(this);
		this.state = {
			editingAddress: false
		}
	}

    /**
	 * Start editing address
     */
	startAddressEditing(){
        this.setState((prevState, props) => ({
            editingAddress: true
        }));
	}

    /**
     * End editing address
     */
    endAddressEditing(e){
		e.preventDefault();

		if(!this.state.editingAddress) return;

        this.setState((prevState, props) => ({
            editingAddress: false
        }));

        var val = this.refAddressField.current.value;
        if(val == this.props.route.address) return;

		this.props.updateRoute(this.props.route.custom_id, {
			address: val,
			coordinates: [],
			status: 'loading'
		})
    }

    /**
     * Render address field
     */
	renderAddressField(){
		if(this.state.editingAddress){
            return (
				<input ref={this.refAddressField} className="routeItem__address routeItem__address-input" type="text" defaultValue={this.props.route.address}/>
            )
		}
        return (
			<div onClick={this.startAddressEditing} className="routeItem__address">
				<div className="fixWidthTextField">
					<span className="fixWidthTextField__text">{this.props.route.address}</span>
				</div>
			</div>
        )
	}

    /**
     * Render remove btn
     */
	renderRemoveBtn(){
        if(this.state.editingAddress){
            return (
				<buttom onClick={this.endAddressEditing} className="routeItem__save"></buttom>
            )
        }
        return	<buttom onClick={() => this.props.removeRoute(this.props.index)} className="routeItem__remove"></buttom>

    }

    /**
     * Render item status
     */
	renderStatus(){
		switch (this.props.route.status){

			case 'loading':
				return <span class="routeItem__status routeItem__status-loading"></span>

			case 'error':
                return <span class="routeItem__status routeItem__status-error"></span>

			default:
				return <span className="routeItem__count">{this.props.index + 1}</span>

		}
	}

	render(){
		return (
			<form onSubmit={this.endAddressEditing} className="routeItem">
				{this.renderStatus()}
				{this.renderAddressField()}
				{this.renderRemoveBtn()}
				<div className="clearfix"></div>
			</form>
		)
	}

}

function mapStateToProps(state){
    return state;
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        removeRoute: removeRoute,
        updateRoute: updateRoute
    },dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(RouteBlock);