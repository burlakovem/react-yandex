/**
 * All app actions
 */


/**
 * Removed route by array id
 * @param id
 * @return {{type: string, id: int}}
 */
export const removeRoute = function(id){
	return {
		type: 'REMOVE_ROUTE',
		id: id
	}
}

/**
 * Add route
 * @param address
 * @return {{type: string, address: string}}
 */
export const addRoute = function(address){
	return {
		type: 'ADD_ROUTE',
        address: address
	}
}

/**
 * Update any route parameters by custom_id
 * @param custom_id
 * @param params
 * @return {{type: string, custom_id: string, params: {}}}
 */
export const updateRoute = function (custom_id, params = {}) {
    return {
        type: 'UPDATE_ROUTE',
        custom_id: custom_id,
        params: params
    }
}

/**
 * Set route address by custom id
 * @param custom_id
 * @param address
 * @return {{type: string, custom_id: string, address: string}}
 */
export const setRouteAddress = function (custom_id, address) {
    return {
        type: 'SET_ROUTE_ADDRESS',
        custom_id: custom_id,
        address: address
    }
}

/**
 * Set route status by custom id
 * @param custom_id
 * @param status
 * @return {{type: string, custom_id: string, status: string}}
 */
export const setRouteStatus = function (custom_id, status) {
    return {
        type: 'SET_ROUTE_STATUS',
        custom_id: custom_id,
        status: status
    }
}

/**
 * Set route coordinates by custom id
 * @param custom_id
 * @param coordinates
 * @return {{type: string, custom_id: string, coordinates: array}}
 */
export const setRouteCoordinates = function (custom_id, coordinates) {
    return {
        type: 'SET_ROUTE_COORDINATES',
        custom_id: custom_id,
        coordinates: coordinates
    }
}

/**
 * Moved route in list
 * @param from
 * @param to
 * @return {{type: string, from: int, to: int}}
 */
export const moveRouteInList = function (from, to) {
    return {
        type: 'MOVE_ROUTE_IN_LIST',
        from: from,
        to: to
    }
}