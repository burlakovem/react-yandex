/**
 * Reducer for routes
 * @param state
 * @param action
 * @return {*}
 */


export default function(state=null, action){

	switch(action.type){

        /**
         * Removed route by array id
         * @param id
         * @return {array}
         */
        case 'REMOVE_ROUTE':
            var arr = [...state];
            arr = arr.filter(function(item,i){
                if(i!=action.id) return true;

            });
            return arr;

        /**
         * Add route
         * @param address
         * @return {array}
         */
		case 'ADD_ROUTE':
			if(!action.address) return state;
			var arr = [...state];
            var postfix1 = Math.abs(Math.ceil(Math.random() * (0 - 100)));
            var postfix2 = Math.abs(Math.ceil(Math.random() * (0 - 100)));
            var date = new Date();
            var custom_id = date.getMilliseconds() + '_' + postfix1 + '_' + postfix2;

            arr.push({
                custom_id: custom_id,
                address: action.address,
                coordinates: [],
                status: 'loading'
            });
			return arr;

        /**
         * Update any route parameters by custom_id
         * @param custom_id
         * @param params
         * @return {array}
         */
		case 'UPDATE_ROUTE':
            var arr = [...state];
			arr = arr.map(function (item) {
				if(item.custom_id == action.custom_id){
                    for(var key in action.params){
                        item[key] = action.params[key];
					}
				}
                return item;
            });
            return arr;

        /**
         * Set route address by custom id
         * @param custom_id
         * @param address
         * @return {array}
         */
        case 'SET_ROUTE_ADDRESS':
            var arr = [...state];
            arr = arr.map(function (item) {
                if(item.custom_id == action.custom_id){
                    item.address = action.address;
                }
                return item;
            });
            return arr;

        /**
         * Set route status by custom id
         * @param custom_id
         * @param status
         * @return {array}
         */
        case 'SET_ROUTE_STATUS':
            var arr = [...state];
            arr = arr.map(function (item) {
                if(item.custom_id == action.custom_id){
                    item.status = action.status;
                }
                return item;
            });
            return arr;

        /**
         * Set route coordinates by custom id
         * @param custom_id
         * @param coordinates
         * @return {array}
         */
        case 'SET_ROUTE_COORDINATES':
            var arr = [...state];
            arr = arr.map(function (item) {
                if(item.custom_id == action.custom_id){
                    item.coordinates = action.coordinates;
                }
                return item;
            });
            return arr;

        /**
         * Moved route in list
         * @param from
         * @param to
         * @return {array}
         */
        case 'MOVE_ROUTE_IN_LIST':
            var arr = [...state];
            var [removed] = arr.splice(action.from, 1);
            arr.splice(action.to, 0, removed);
            return arr;

        /**
         * Default action
         * @return {array}
         */
		default:
            return state || [];

	}

}