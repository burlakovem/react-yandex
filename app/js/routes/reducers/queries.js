/**
 * Last queries for cookie history
 * @param state
 * @param action
 * @return {*}
 */


export default function (state=null, action) {

    switch (action.type){

        /**
         * Add route in coockie history
         */
        case 'ADD_ROUTE':
            if(!action.address) return state;
            if(state.indexOf(action.address) != -1) return state;

            var arr = [...state];

            if(arr.length > 9){
                arr.splice(arr.length-1,1);
            }

            arr.unshift(action.address);
            window.cookie.setCookie('lastQueries', JSON.stringify(arr), {
                path: '/',
                expires: 2592000
            });
            return arr;

        default:
            var cookie = window.cookie.getCookie('lastQueries') || false;
            return state || JSON.parse(cookie) || [];

    }

}