/**
 * Combined reducers
 */

import { combineReducers } from 'redux';
import routes from './routes';
import queries from './queries';

const ALL_REDUCERS = combineReducers({
    routes: routes,
    queries: queries
});

export default ALL_REDUCERS;

