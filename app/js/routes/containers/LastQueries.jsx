/**
 * Last queries
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addRoute } from '../actions';


class LastQueries extends React.Component{
    constructor(props){
        super();
    }
    render(){
        var self = this;
        return (
            <div className="searchWindow search__lastSearch">
                <div className="searchWindow__title">Последние запросы:</div>

                {
                    this.props.queries.map(function (item) {
                        return (
                            <div onClick={() => {
                                self.props.addRoute(item);
                                self.props.toggleViewLastQueries();
                            }} className="searchWindow__item">
                                <div className="fixWidthTextField">
                                    <span className="fixWidthTextField__text">{item}</span>
                                </div>
                            </div>
                        )
                    })
                }

            </div>
        )
    }
}

function mapStateToProps(state){
    return state;
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        addRoute: addRoute
    },dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(LastQueries);