/**
 * Yandex map block
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { YMaps, Map, Placemark, Polyline } from 'react-yandex-maps';
import { setRouteCoordinates, setRouteStatus, setRouteAddress, updateRoute } from '../actions';


class Ymap extends React.Component{
    constructor(props){
        super();
        this.onDragEnd = this.onDragEnd.bind(this);
        this.state = {
            center: [55.76, 37.64],
            zoom: 10,
        }
    }

    /**
     * Setting src to ymaps object
     * @param ymaps
     */
    setYmapsThis(ymaps){
        this.ymaps = ymaps;
    }

    /**
     * Getting coordinates by address
     * @param address
     * @param callback
     * @param error
     */
    getCoordinates(
        address,
        callback = function (data) {console.log(data)},
        error = function (data) {console.log('error',data)}
    ) {
        this.getGeocode(
            address,
            function(res){
                return callback(res.boundedBy[0])
            },
            error
        )
    }

    /**
     * Getting address by coordinates
     * @param coordinates
     * @param callback
     * @param error
     */
    getAddress(
        coordinates,
        callback = function (data) {console.log(data)},
        error = function (data) {console.log('error',data)}
    ) {
        this.getGeocode(
            coordinates,
            function(res){
                return callback(res.description)
            },
            error
        )
    }

    /**
     * Getting all geocode parameters
     * @param addrr_coord
     * @param callback
     * @param error
     */
    getGeocode(
        addrr_coord,
        callback = function (data) {console.log(data)},
        error = function (data) {console.log('error',data)}
    ){
        var myGeocoder = this.ymaps.geocode(addrr_coord);
        myGeocoder.then(
            function (res) {
                callback(res.geoObjects.get(0).properties.getAll());
            },
            error
        );
    }

    /**
     * Set new address in route by custom_id
     * @param custom_id
     * @param coordinates
     */
    onDragEnd(custom_id, coordinates){
        var self = this;
        self.props.setRouteStatus(custom_id, 'loading');
        self.getAddress(
            coordinates,
            function (res) {

                self.props.updateRoute(custom_id, {
                    address: res,
                    coordinates: coordinates,
                    status: 'ok'
                });

            },
            function (res) {
                console.log(res);
                self.props.setRouteStatus(route.custom_id, 'error');
            }
        )
    }

    render(){
        var self = this;
        return (
            <YMaps onApiAvaliable={ymaps => this.setYmapsThis(ymaps)}>
                <Map
                    state={this.state}
                    options={{
                        suppressMapOpenBlock: true
                    }}
                >

                    {self.props.routes.map(
                        function(route){

                            if(route.coordinates.length || route.status == 'error'){
                                return (
                                    <Placemark
                                        onDragEnd={(e) => {
                                            self.onDragEnd(
                                                route.custom_id,
                                                e.originalEvent.target.geometry._coordinates
                                            );
                                        }}
                                        options={{
                                            draggable: true
                                        }}
                                        geometry={{
                                            coordinates: route.coordinates
                                        }}
                                        properties={{
                                            balloonContent: route.address
                                        }}
                                    />
                                )
                            }

                            self.getCoordinates(
                                route.address,
                                function (res) {

                                    self.props.updateRoute(route.custom_id, {
                                        coordinates: res,
                                        status: 'ok'
                                    });

                                    self.setState((prevState, props) => ({
                                        center: res
                                    }));

                                },
                                function (res) {
                                    console.log(res);
                                    self.props.setRouteStatus(route.custom_id, 'error');
                                }
                            );

                        }
                    )}

                    <Polyline
                        geometry={{
                            coordinates: self.props.routes.map((route) => route.coordinates)
                        }}
                    />

                </Map>
            </YMaps>
        )
    }
}

function mapStateToProps(state){
    return state;
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        setRouteCoordinates: setRouteCoordinates,
        setRouteStatus: setRouteStatus,
        setRouteAddress: setRouteAddress,
        updateRoute: updateRoute
    },dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Ymap);