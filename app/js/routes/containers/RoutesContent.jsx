/**
 * Routes list
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import RouteBlock from '../components/RouteBlock';
import { moveRouteInList } from '../actions';


class RoutesContent extends React.Component{

	constructor(props){
		super();
        this.onDragEnd = this.onDragEnd.bind(this);
	}

    /**
	 * Moved route in list
     * @param result
     */
    onDragEnd(result){
        if (!result.destination) {
            return;
        }
        this.props.moveRouteInList(result.draggableId, result.destination.index);
	}

    render(){

		return (
			<DragDropContext onDragEnd={this.onDragEnd}>
				<Droppable droppableId="droppable">
					{(provided, snapshot) => (
						<div className="routesItems" ref={provided.innerRef}>
                            {this.props.routes.map(

								function(route,i) {
									return (
										<Draggable draggableId={i} index={i} key={i} >
                                            {(provided, snapshot) => (
												<div className="routesItems__item" ref={provided.innerRef}
                                                     {...provided.draggableProps}
                                                     {...provided.dragHandleProps}>

													<RouteBlock index={i} key={i} route={route} />

												</div>
                                            )}
										</Draggable>
									)

								})

							}
						</div>
					)}
				</Droppable>
			</DragDropContext>
		)
	}

}

function mapStateToProps(state){
	return state;
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        moveRouteInList: moveRouteInList
    },dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(RoutesContent);