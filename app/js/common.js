$(document.body).ready(function () {

    var COOKIE = {
        colorSwitch: JSON.parse(window.cookie.getCookie('colorSwitch') || false)
    }

    function toggleColorSwitch() {
        var el = $("#color-switcher");
        el.toggleClass('switcher-night');
        el.toggleClass('switcher-switch');
        $(document.body).toggleClass('color-night');
    }

    if(COOKIE.colorSwitch){
        toggleColorSwitch();
    }

    $("#color-switcher").click(function (e) {
        e.preventDefault();
        toggleColorSwitch();

        window.cookie.setCookie('colorSwitch', JSON.stringify(!COOKIE.colorSwitch), {
            path: '/',
            expires: 2592000
        });
    });
});