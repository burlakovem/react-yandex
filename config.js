class Config{
	constructor(){
        this.src = "app";
		this.export = "dist";
        this.jsDir = "/js";
        this.cssDir = "/css";
        this.sassDir = this.cssDir + "/sass";
        this.imgDir = "/img";
        this.min = ".min";
        this.compile = ".compile";
		this.jsIncludesMore = [
            {
                "src": this.src + this.jsDir + "/routes/index.js",
                "name": "routes"
            }
		];
		this.jsIncludes = this.jsIncludesMore.map( (item) => item.src );
		this.cssIncludesMore = [
			{
				"src": this.src + this.sassDir+ "/main.scss",
				"name": "main"
			}
		];
        this.cssIncludes = this.cssIncludesMore.map( (item) => item.src );
	}
}
module.exports=new Config;